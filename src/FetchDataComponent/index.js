import React, {useEffect, useState} from "react";
import {useFetch} from "../hook/useFetch";

export const FetchDataComponent = () => {
  const [data, setData] = useState([]);
  // const [url, setUrl] = useState('google.com');

  // const {loading, data, error} = useFetch(url)
  // useEffect(() => {
  //   fetch('google.com')
  //     .then(res => {
  //       setData([1,2,3,4,5,6]);
  //       console.log("res", res)
  //     })
  //     .catch(err => console.log("error", err));
  // }, [])
  // console.log("data", data);

  useEffect(() => {

    const timerId = setTimeout(() => {
      setData(prevState => {
        console.log("Tick",Math.floor(Math.random()*100))
        return ([...prevState, Math.floor(Math.random()*100)])
      });
    }, 4000);
    return () => clearTimeout(timerId)
  }, [])

  return (
    <>
      <div className="col-12 mb-1">
        <h3>Fetch data</h3>
      </div>
      {/*<div className="col-12 mb-1">*/}
      {/*  <button className="btn btn-primary" onClick={() => setUrl('amazon.com')}>Get data from amazon</button>*/}
      {/*</div>*/}
      {/*<div className="col-12  mb-1">*/}
      {/*  <button className="btn btn-info" onClick={() => setUrl('ebay.com')}>Get data from eBay</button>*/}
      {/*</div>*/}
    </>
  )
}