import React, {useState} from "react";
import {FetchDataComponent} from "../FetchDataComponent";
export const App = () => {
  const [isDisplay, setDisplayStatus] = useState(true);
  return (
    <div className="container">
      <div className="row">
        {
          isDisplay && <FetchDataComponent />
        }
        <div className="col-12">
          <button onClick={() => setDisplayStatus(false)} className="btn btn-danger">
            Hide fetch data component
          </button>
        </div>
      </div>
    </div>
  );
}
